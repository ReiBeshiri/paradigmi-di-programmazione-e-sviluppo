import lab01.tdd.CircularList;
import lab01.tdd.*;
import org.junit.jupiter.api.Test;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * The test suite for testing the CircularList implementation
 */
public class CircularListTest {


    CircularList circularList = new CircularListImpl();

    //va a buon fine perchè la prima implementazione ha i metodi vuoti e isEmpty returna False.
    @Test
    void isEmpty(){
        assertEquals(true, circularList.isEmpty());
    }

    @Test
    void sizeBeforeFirstAdd(){
        assertEquals(0, circularList.size());
    }

    @Test
    void addToList(){
        circularList.add(1);
        assertEquals(false, circularList.isEmpty());
    }

    @Test
    void sizeAfterFirstAdd(){
        circularList.add(1);
        assertEquals(1, circularList.size());
    }

    @Test
    void getNextElement(){
        circularList.add(1);
        circularList.add(2);
        assertEquals(Optional.of(2), circularList.next());
    }

    @Test
    void getNextElementTwice(){
        circularList.add(1);
        circularList.add(2);
        circularList.add(3);
        circularList.next();
        assertEquals(Optional.of(3), circularList.next());
    }

    @Test
    void getPrevElement(){
        circularList.add(1);
        circularList.add(2);
        assertEquals(Optional.of(2), circularList.previous());
    }

    @Test
    void getPrevElementTwice(){
        circularList.add(1);
        circularList.add(2);
        circularList.next();
        assertEquals(Optional.of(1), circularList.previous());
    }

    @Test
    void resetList(){
        circularList.add(1);
        circularList.add(2);
        circularList.add(2);
        circularList.add(2);
        assertEquals(false, circularList.isEmpty());
        circularList.reset();
        assertEquals(true, circularList.isEmpty());
    }


    @Test
    void testStrategyEven(){
        circularList.add(1);
        circularList.add(2);
        assertEquals(2, circularList.next(new Even()).get());
    }

    @Test
    void testStrategyOdd(){
        circularList.add(1);
        circularList.add(2);
        assertEquals(1, circularList.next(new Odd()).get());
    }
}
