package lab01.tdd;

public class Even implements SelectStrategy {
    @Override
    public boolean apply(int element) {
        return element%2 == 0;
    }
}
