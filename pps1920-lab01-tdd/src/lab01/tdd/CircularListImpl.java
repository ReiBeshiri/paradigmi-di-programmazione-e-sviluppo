package lab01.tdd;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class CircularListImpl implements CircularList {

    private final List<Integer> circularList = new LinkedList<>();
    private int pointer = 0;

    @Override
    public void add(int element) {
        this.circularList.add(element);
    }

    @Override
    public int size() {
        return this.circularList.size();
    }

    @Override
    public boolean isEmpty() {
        return this.circularList.isEmpty();
    }

    @Override
    public Optional<Integer> next() {
        this.pointer = (this.pointer+1) % circularList.size();
        return Optional.of(circularList.get(pointer));
    }

    @Override
    public Optional<Integer> previous() {
        if(this.pointer==0){this.pointer = circularList.size()-1;}
        else {this.pointer--;}
        return Optional.of(circularList.get(pointer));
    }

    @Override
    public void reset() {
        this.circularList.clear();
        this.pointer = 0;
    }

    @Override
    public Optional<Integer> next(SelectStrategy strategy) {
        Integer tmpPointer = pointer;
        Integer res = null;
        do {
            if(strategy.apply(next().get())){
                res = circularList.get(pointer);
            }
        }while(tmpPointer != pointer || res == null);
        return Optional.of(res);
        }

}
