package lab01.example.model;

public class SimpleBankAccountWithATM extends SimpleBankAccount implements BankAccountWithATM {

    public SimpleBankAccountWithATM(final AccountHolder holder, final double balance) {
        super(holder, balance);
    }

    @Override
    public void depositWithATM(int usrID, double amount) {
        deposit(usrID,amount-1);
    }

    @Override
    public void withdrawWithATM(int usrID, double amount) {
        withdraw(usrID, amount+1);
    }
}
