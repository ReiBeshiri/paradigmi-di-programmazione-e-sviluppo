package u02

object EsLab02 extends App{
  //3a functions
  /*
  val evenOrOdd: Int => String = {
    case n if n%2==0 => "Even"
    case _ => "Odd"
  }

  System.out.println(evenOrOdd(4))
  System.out.println(evenOrOdd(5))

  def getEvenOrOdd(i: Int) : String = i match {
    case even if i%2==0 => "even"
    case _ => "odd"
  }

  System.out.println(getEvenOrOdd(4))
  System.out.println(getEvenOrOdd(5))
  */

  //3b predicates
  /*
  val neg: (String => Boolean) => (String => Boolean) = p => (s => !p(s))
  val notEmpty = neg(s => s == "")

  System.out.println(notEmpty("ciao"))                  //true
  System.out.println(notEmpty(""))                      //false
  System.out.println(notEmpty("ciao") && !notEmpty("")) //true
  */

  //4 Currying
  /*
  def curryingFun(x : Int)(y : Int)(z : Int) : Boolean = x <= y && y <= z
  System.out.println(curryingFun(1)(2)(3))

  def noCurryingFun(x:Int, y:Int, z:Int) : Boolean = x <= y && y <= z
  System.out.println(noCurryingFun(1,2,3))

  val curriedVal : Int => Int => Int => Boolean = x => y => z => (x <= y) && (y <= z)
  System.out.println(curriedVal(1)(2)(3))

  val noCurryingVal: (Int, Int, Int) => Boolean = (x:Int, y:Int, z:Int) => (x <= y) && (y <= z)
  System.out.println(noCurryingVal(1,2,3))
  */

  //5 composition
  /*
  def compose(f: Int => Int, g: Int => Int) = ((f: Int => Int) compose ((g: Int => Int)))
  System.out.println(compose(_-1,_*2)(5)) // f(g(5)) f=x-1, g=x*2
   */

  //6 recursion
  /*
  def fibonacci(i: Int): Int = i match {
    case zero if i == 0 => 0
    case uno  if i == 1 => 1
    case _              => fibonacci(i-1) + fibonacci(i-2)
  }
  System.out.println(fibonacci(7))
  */

  //7 data structure
  /*
  sealed trait Shape
  object Shape {
    case class Circle(r: Double)          extends Shape
    case class Square(l:Double)           extends Shape
    case class Rect(l1:Double, l2:Double) extends Shape

    def perimeter(s:Shape) : Double = s match {
      case Circle(r) => 2 * Math.PI
      case Square(l) => l * 4
      case Rect(l1, l2) => l1 *2 + l2* 2
    }

    def area(s:Shape) : Double = s match {
      case Circle(r) => Math.pow(r, 2) * Math.PI
      case Square(l) => Math.pow(l, 2)
      case Rect(l1, l2) => l1 * l2
    }
  }

  import Shape._
  System.out.println(perimeter(Circle(2)))
  System.out.println(perimeter(Square(2)))
  System.out.println(perimeter(Rect(2, 3)))
  System.out.println(area(Circle(2)))
  System.out.println(area(Square(2)))
  System.out.println(area(Rect(2, 3)))
  */

  //8 Options
  /*
  sealed trait Option[A] // An Optional data type
  object Option {
    case class None[A]() extends Option [A]
    case class Some[A](a: A) extends Option [A]

    def filter[A](opt: Option[A], f: A => Boolean) : Option[A] = opt match {
      case Some(a) if f(a) == true => Some(a)
      case _ => None()
    }

    def map[A, B >: Boolean](opt: Option[A], f: A => Boolean): Option[B] = opt match {
      case Some(a) if f(a) == true => Some(true)
      case _ => None()
    }
  }

  import Option._
  var s1: Option[Int] = Some(5)
  var s2: Option[Int] = None()
  var s3: Option[String] = Some("a")
  var s4: Option[String] = Some("c")
  System.out.println(filter(s1, (n:Int) => n > 4))
  System.out.println(filter(s2, (n:Int) => n > 4))
  System.out.println(filter(s3, (s:String) => s == "a"))
  System.out.println(filter(s4, (s:String) => s == "a"))
  System.out.println(map(s1, (n:Int) => n > 4))
  System.out.println(map(s2, (n:Int) => n > 4))
  System.out.println(map(s3, (s:String) => s == "a"))
  System.out.println(map(s4, (s:String) => s == "a"))
  */
}
